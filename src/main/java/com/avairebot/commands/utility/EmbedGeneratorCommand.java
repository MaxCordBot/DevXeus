package com.avairebot.commands.utility;

import com.avairebot.AvaIre;
import com.avairebot.commands.CommandMessage;
import com.avairebot.contracts.commands.Command;
import com.avairebot.contracts.commands.CommandGroup;
import com.avairebot.contracts.commands.CommandGroups;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import javax.annotation.Nonnull;
import java.awt.*;
import java.util.*;
import java.util.List;


public class EmbedGeneratorCommand extends Command {


    private EmbedBuilder eb = new EmbedBuilder();

    private int fNum = 1;

    private TextChannel tc = null;

    public EmbedGeneratorCommand(AvaIre avaire) {
        super(avaire);
    }

    @Override
    public String getName() {
        return "Embed Generator Command";
    }

    @Override
    public String getDescription() {
        return "Create an embed in a specified channel, or in the channel you ran the command in.";
    }

    @Override
    public List<String> getUsageInstructions() {
        return Collections.singletonList("`:command [action] [action]` - Starts the embed-generator");
    }

    @Override
    public List<String> getExampleUsage() {
        return Collections.singletonList(
            "`:command edit color` - Edits the color by following the prompts from the bot"
        );
    }

    @Override
    public List<String> getMiddleware() {
        return Arrays.asList(
            "isModOrHigher",
            "throttle:guild,1,5"
        );
    }

    @Override
    public List<Class<? extends Command>> getRelations() {
        return Collections.singletonList(ChannelIdCommand.class);
    }

    @Nonnull
    @Override
    public List<CommandGroup> getGroups() {
        return Collections.singletonList(CommandGroups.INFORMATION);
    }

    @Override
    public List<String> getTriggers() {
        return Arrays.asList("embed-generator", "e-g", "eg");
    }

    @Override
    public boolean onCommand(CommandMessage context, String[] args) {

        if (args.length < 1) {
            context.makeError("Please enter in a argument.").queue();
            return false;
        }

        eb.setAuthor(context.getAuthor().getName(), null, context.getAuthor().getAvatarUrl());

        switch (args[0]) {
            case "a":
            case "add":
                return runAddArgument(context,args);

            case "e":
            case "edit":
                return runEditArgument(context, args);
            case "s":
            case "send":
                return runSendArgument(context);
            }
        return false;
    }


    private boolean runAddArgument(CommandMessage context, String[] args) {
        if (args.length < 2) {
            context.makeError("You're missing the thing you want to add.").queue();
            return false;
        }

        StringBuilder name = new StringBuilder();
        StringBuilder value = new StringBuilder();

        switch (args[1]) {
            case "f":
            case "field":
                context.makeInfo("Please enter a **name** for field " + fNum + "\n Response 1/3").queue();

                avaire.getWaiter().waitForEvent(GuildMessageReceivedEvent.class, event -> event.getAuthor().equals(context.getAuthor()) && event.getChannel().equals(context.getChannel()), p -> {
                    String t = p.getMessage().getContentRaw();
                    name.append(t);
                    context.makeSuccess("The name of field " + fNum + " has been set to: **" + t + "**\n Response 1/3").queue();
                    context.makeInfo("Please enter a **value** for field " + fNum + "\n Response 2/3").queue();
                    avaire.getWaiter().waitForEvent(GuildMessageReceivedEvent.class, event -> event.getAuthor().equals(context.getAuthor()) && event.getChannel().equals(context.getChannel()), q -> {
                        String s = q.getMessage().getContentRaw();
                        value.append(s);
                        context.makeSuccess("The value of field " + fNum + " has been set to: **" + s + "**\n Response 2/3").queue();
                        context.makeInfo("Should field " + fNum + " be **inline**?" + "\n Response 3/3").queue();
                        avaire.getWaiter().waitForEvent(GuildMessageReceivedEvent.class, event -> event.getAuthor().equals(context.getAuthor()) && event.getChannel().equals(context.getChannel()), r -> {
                            String j = r.getMessage().getContentRaw();
                            eb.addField(name.toString(), value.toString(), j.equals("true"));
                            context.makeSuccess("The field's inline property has been set to: **" + j.equals("true") + "**\n Response 3/3").queue();
                            context.makeSuccess("Field " + fNum + "'s name has been set to **" + name + "**, its value has been set to **" + value + "** and the inline property has been set to **" + eb.getFields().get(fNum-1).isInline() + "**!").queue();
                            fNum++;
                        });
                    });
                });
                break;

            case "t":
            case "timestamp":
                eb.setTimestamp(new Date().toInstant());
                context.makeSuccess("Added a **timestamp** to this embed!").queue();
                break;

            case "image":
                context.makeInfo("Please enter the **URL** for the **image**!").queue();

                avaire.getWaiter().waitForEvent(GuildMessageReceivedEvent.class, event -> event.getAuthor().equals(context.getAuthor()) && event.getChannel().equals(context.getChannel()), p -> {
                    String t = p.getMessage().getContentRaw();
                    eb.setImage(t);
                    context.makeSuccess("The **image** has been set to: **" + t + "**!").queue();
                });

                break;

            case "thumbnail":
                context.makeInfo("Please enter the **URL** for the **thumbnail**!").queue();
                avaire.getWaiter().waitForEvent(GuildMessageReceivedEvent.class, event -> event.getAuthor().equals(context.getAuthor()) && event.getChannel().equals(context.getChannel()), p -> {
                    String t = p.getMessage().getContentRaw();
                    eb.setImage(t);
                    context.makeSuccess("The **thumbnail image** has been set to: **" + t + "**!").queue();
                });
                break;
        }

        return true;
    }




    private boolean runEditArgument(CommandMessage context, String[] args) {
        if (args.length < 2) {
            context.makeError("You're missing the thing you want to edit.").queue();
            return false;
        }


        switch (args[1]) {
            case "t":
            case "title":

                context.makeInfo("Please enter the value for the **title**!").queue();

                avaire.getWaiter().waitForEvent(GuildMessageReceivedEvent.class, event -> event.getAuthor().equals(context.getAuthor()) && event.getChannel().equals(context.getChannel()), p -> {
                    String t = p.getMessage().getContentRaw();
                    eb.setTitle(t);
                    context.makeSuccess("The title has been set to: **" + t + "**!").queue();
                });
                break;


            case "channel":

                context.makeInfo("Please enter the **channel ID**!").queue();

                avaire.getWaiter().waitForEvent(GuildMessageReceivedEvent.class, event -> event.getAuthor().equals(context.getAuthor()) && event.getChannel().equals(context.getChannel()), p -> {

                    tc = context.getGuild().getTextChannelById(p.getMessage().getContentRaw());
                    assert tc != null;
                    context.makeSuccess("The **channel** has been set to: " + "<#" + tc.getId() + ">").queue();
                });

                break;

            case "description":

                context.makeInfo("Please enter the **description**!").queue();

                avaire.getWaiter().waitForEvent(GuildMessageReceivedEvent.class, event -> event.getAuthor().equals(context.getAuthor()) && event.getChannel().equals(context.getChannel()), p -> {

                    String t = p.getMessage().getContentRaw();
                    eb.setDescription(t);
                    context.makeSuccess("The description has been set to: **" + t + "**!").queue();
                });

                break;

            case "color":

                context.makeInfo("Please enter the **color**!").queue();

                avaire.getWaiter().waitForEvent(GuildMessageReceivedEvent.class, event -> event.getAuthor().equals(context.getAuthor()) && event.getChannel().equals(context.getChannel()), p -> {
                    String t = p.getMessage().getContentRaw();
                    try {
                        eb.setColor((Color) Class.forName("java.awt.Color").getField(t.toLowerCase()).get(null));
                    } catch (Exception e) {
                        context.makeError("Color not found!").queue();
                        return;
                    }
                    context.makeSuccess("The description has been set to: **" + t + "**!").queue();
                });

                break;

            case "footer":
                context.makeInfo("Please enter the **footer**!").queue();

                avaire.getWaiter().waitForEvent(GuildMessageReceivedEvent.class, event -> event.getAuthor().equals(context.getAuthor()) && event.getChannel().equals(context.getChannel()), p -> {
                    String t = p.getMessage().getContentRaw();
                    eb.setFooter(t);

                    context.makeSuccess("The description has been set to: **" + t + "**!").queue();
                });
        }
        return true;
    }

    private boolean runSendArgument(CommandMessage context) {

        eb.setFooter(context.getMember().getNickname());
        try {
            if (tc == null) {
                context.getChannel().sendMessage(eb.build()).queue();
                tc = context.getChannel();
            } else {
                tc.sendMessage(eb.build()).queue();
            }
        } catch (IllegalStateException e) {
            context.makeError("I can't send an empty embed!").queue();
            return false;
        }

        context.makeSuccess("Your embed has been send to: <#" + tc.getId() + ">").queue();

        fNum = 1;
        tc = null;
        eb = new EmbedBuilder();
        return true;

    }
}
